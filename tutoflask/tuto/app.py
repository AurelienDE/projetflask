#! /usr/bin/env python3
from flask import Flask
import os.path

app=Flask(__name__)
app.debug=True

from flask.ext.script import Manager
manager=Manager(app) 

from flask.ext.bootstrap import Bootstrap
Bootstrap(app)

def mkpath(p):
	return os.path.normpath(os.path.join(os.path.dirname(__file__),p))
	

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI']=('sqlite:///'+mkpath('../tuto.db'))
app.config['SECRET_KEY'] = "abcdefg"
db=SQLAlchemy(app)

app.config['WHOOSH_BASE'] = 'C:/WinPython/projetG/projetflask/tutoflask'

#app.config['CSRF_ENABLED'] = False
#app.config['SECRET_KEY '] = "5ee4c3db-c8c9-428a-8556-d0fd017a6504"

MAX_SEARCH_RESULTS=10

#app.config['WTF_CSRF_ENABLED']=True
#app.config['WTF_CSRF_SECRET_KEY']='abcdefg'
#app.config['WTF_CSRF_SECRET_KEY']='63fe9cd1-1b84-4368-910d-570f5194b8fe'


from flask.ext.login import LoginManager
login_manager = LoginManager(app)

app.config['UPLOAD_IMG_VIN_FOLDER'] = 'tuto/static/images'

login_manager.login_view="login"

