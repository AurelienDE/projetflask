# -*- coding: utf8 -*-
from .app import manager,db

@manager.command
def loaddb(filename):
	'''Creates the tables and populates them whith data.'''
	
	#création de toute les tables
	db.create_all()
	
	#Chargement de notre jeu de données
	import yaml
	vins= yaml.load(open(filename))
	
	
	#import des modéles

	from .models import VIN,pannier

	
	#deuxiéme passe : création de tous les vins
	for b in vins:
		o=VIN(category=b["category"],
			   country=b["country"],
			   image=b["image"],
			   name=b["name"],
			   quantity=b["quantity"],
			   region=b["region"],
			   varietal=b["varietal"],
			   vintage=b["vintage"])
			   
		db.session.add(o)
	db.session.commit()


@manager.command	
def syncdb():
	db.create_all()
	
@manager.command
def newuser(username,password,adressemail,pseudo,prenom,nom):
	from .models import User
	from hashlib import sha256
	m=sha256()
	m.update(password.encode())
	u=User(username=username,password=m.hexdigest(),adressemail=adressemail,grade="admin",pseudo=pseudo,prenom=prenom,nom=nom)
	db.session.add(u)
	db.session.commit()

@manager.command
def add_pannier(name,idvin,pagevin,namevin,imagevin):
	from .models import pannier
	p=pannier(name=name,idvin=idvin,pagevin=pagevin,namevin=namevin,imagevin=imagevin)
	db.session.add(p)
	db.session.commit()

@manager.command
def supprimer_pannier(id):
	from .models import pannier
	p=pannier.query.get_or_404(id)
	db.session.delete(p)
	db.session.commit()

@manager.command
def reset_pannier(name):
	from .models import pannier,get_sample_pannier
	a=get_sample_pannier()
	for x in a:
		if x.name==name:
			p=pannier.query.get_or_404(x.id)
			db.session.delete(p)
			db.session.commit()

@manager.command
def newVin(category,country,image,name,quantity,region,varietal,vintage):
	v=VIN(category=category,image=image,quantity=quantity,region=region,varietal=varietal,vintage=vintage)
	db.session.add(v)
	db.session.commit()

@manager.command
def supprimerVin(id):
	from .models import VIN,get_sample,pannier
	p=VIN.query.get_or_404(id)
	db.session.delete(p)

	db.session.commit()


			# db.session.expire_all()
			# db.session.refresh(test)
			# db.session.commit()

@manager.command
def get_sample12(number):
	from .models import VIN,get_sample
	#a=db.session.query(VIN).filter(VIN.id > number-12)
	a=db.session.query(VIN).slice(number-13,number-1)
	tab=[]
	for x in a:
		tab+=[x]
	return tab




