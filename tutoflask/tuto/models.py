# -*- coding: utf8 -*-
from .app import db
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
#from .app import login_manager

class VIN(db.Model):
	id=db.Column(db.Integer,primary_key=True)
	category=db.Column(db.String(100))
	country=db.Column(db.String(100))
	image=db.Column(db.String(300))
	name=db.Column(db.String(300))
	quantity=db.Column(db.Integer)
	region=db.Column(db.String(100))
	varietal=db.Column(db.String(100))
	vintage=db.Column(db.String(100))
	def __repr__(self):
		return "<VIN (%d) %s>" % (self.id, self.name)
		
	#~ 
	#~ author_id=db.Column(db.Integer, db.ForeignKey("author.id"))
	#~ author=db.relationship("Author",backref=db.backref("books",lazy="dynamic"))

def get_sample():
	return VIN.query.all()

def nb_vin():
	return VIN.query.count()



def get_sample10(name):
	test=get_sample()
	cpt=0
	tab=[]
	for x in test:	
		if x.name==name and cpt==1:
			tab+=[VIN.query.get_or_404(name)]
		if x.name==name and cpt==0:
			tab+=[VIN.query.get_or_404(name)]
			cpt+=1
	return tab

def get_sample3(number):
	if VIN.query.count()<number:
		return False
	return True
def get_sample4(number):
	if VIN.query.count()-13<number:
		return False
	return True
def get_sample5(number):
	if VIN.query.count()-26<number:
		return False
	return True
def get_sample6(number):
	if VIN.query.count()-38<number:
		return False
	return True
	
	
def get_vin(id):
	return VIN.query.get_or_404(id)
	


def get_last_vin():
	return VIN.query.count()
def get_nbpage():
	return (VIN.query.count()//12)+1
#~ def get_selectbook(id):
	#~ return Book.query.get_or_404(id)
	#~ 
	
#~ def get_author(id):
	#~ return Author.id

# @login_manager.user_loader
# def load_user(username):
# 	return User.query.get(username)

from .app import login_manager

@login_manager.user_loader
def load_user(username):
	return User.query.get(username)

from flask.ext.login import UserMixin

class User(db.Model,UserMixin):
	username= db.Column(db.String(50),primary_key=True)
	password= db.Column(db.String(64))
	adressemail= db.Column(db.String(64))
	grade=db.Column(db.String(64))
	pseudo=db.Column(db.String(64))
	prenom=db.Column(db.String(64))
	nom=db.Column(db.String(64))
	
	def get_id(self):
		return self.username
	
from .app import login_manager

class pannier(db.Model):
	id=db.Column(db.Integer,primary_key=True)
	name=db.Column(db.String(300))
	idvin=db.Column(db.String(100))
	pagevin=db.Column(db.String(100))
	namevin=db.Column(db.String(100))
	imagevin=db.Column(db.String(300))

def get_sample_pannier():
	return pannier.query.all()
