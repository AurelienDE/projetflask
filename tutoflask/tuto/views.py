# -*- coding: utf8 -*-

from .app import app,db
from flask import render_template, url_for, redirect,flash
from flask.ext.wtf import Form
from wtforms import HiddenField, StringField,SelectField,FileField
from flask.ext.login import login_required

from .commands import get_sample12,add_pannier,supprimer_pannier,reset_pannier,supprimerVin


from .models import nb_vin,get_sample10,get_nbpage,get_sample,get_vin,VIN,get_last_vin,get_sample3,get_sample4,get_sample5,get_sample6,get_sample_pannier

import pprint

class RechercheForm(Form):
	npage=StringField('Page')
	
@app.route("/recherchepage",methods=['GET','POST'])
def recherchepage():
	form = RechercheForm()
	return redirect(url_for('home/'+Form.npage.data))

@app.route("/")
@app.route("/<int:num_page>")
def home(num_page=1):
	form=SearchForm()
	num_page=int(num_page)
	number=13	
	if num_page!=1:	
		number=(number*num_page)	
		for x in range(1,num_page):
			number=number-1
	#vins=get_sample2(number)

	return render_template("home.html",nbvin=nb_vin(),fs=form,nbpage=get_nbpage(),title="Nos vins",nombre=get_sample3(number),nombre2=get_sample4(number),nombre3=get_sample5(number),nombre4=get_sample6(number),pannier=get_sample_pannier(),vins=get_sample12(number),num=num_page)



@app.route("/about")
def about():
	fs = SearchForm()
	return render_template("about.html", title="about",fs=fs)


@app.route("/gestionvins")
def gerervin():
	fs = SearchForm()
	return render_template("gestionvin.html", title="GestionVins",fs=fs)


from flask.ext.wtf import Form
from wtforms import HiddenField, StringField,SelectField,FileField,TextField

from wtforms.validators import DataRequired,Required

class VINForm(Form):
	id=HiddenField('id')
	page=HiddenField('page')
	name=StringField('Nom',validators=[DataRequired()])
	country=StringField('Country',validators=[DataRequired()])
	image=StringField('Image',validators=[DataRequired()])
	category=StringField('Category',validators=[DataRequired()])
	quantity=StringField('Quantity',validators=[DataRequired()])
	region=StringField('Region',validators=[DataRequired()])
	varietal=StringField('Varietal',validators=[DataRequired()])
	vintage=StringField('Vintage')

@app.route("/edit/vin/<int:id>/<page>")
@app.route("/edit/vin/<int:id>")
@login_required
def edit_vin (id,page):
	page2=page
	a = get_vin(id)
	fs = SearchForm()
	f = VINForm(id=a.id ,page=page2, name=a.name, image=a.image,country=a.country,category=a.category,quantity=a.quantity,region=a.region,varietal=a.varietal,vintage=a.vintage)
	return render_template("edit_vin.html",fs=fs,vin=a,form=f)


@app.route("/vin/<int:id>/<page>")
@app.route("/vin/<int:id>")
def affiche_vin (id,page):
	a = get_vin(id)
	fs = SearchForm()
	return render_template("affichevin.html",pannier=get_sample_pannier(),vin=a,title="Affichage vin",fs=fs, numpage=page, x=id)
	
@app.route("/save/vin/",methods=("POST",))
def save_vin():
	a=None
	f= VINForm()
	fs = SearchForm()
	if f.validate_on_submit():
		id=int(f.id.data)
		a= get_vin(id)
		a.name=f.name.data
		a.country=f.country.data
		a.image=f.image.data
		a.category=f.category.data
		a.quantity=f.quantity.data
		a.region=f.region.data
		a.varietal=f.varietal.data
		a.vintage=f.vintage.data
		db.session.commit()
		#~ return redirect("http://127.0.0.1:5000/"+f.page.data)
		return redirect(url_for('affiche_vin',id=a.id, page=f.page.data))
	a=get_vin(int(f.id.data))
	return render_template("edit_vin.html",vin=a, form=f,fs=fs)
	
from flask.ext.login import login_required

from wtforms import PasswordField
from .models import User
from hashlib import sha256

class LoginForm(Form):
	username=StringField('Username',[Required()])
	password=PasswordField('Password',[Required()])
	adressemail= StringField('adressemail')
	pseudo=StringField('pseudo')
	prenom=StringField('prenom')
	nom=StringField('nom')
	next= HiddenField()
	
	def get_authenticated_user(self):
		user= User.query.get(self.username.data)
		if user is None:
			return None
		m=sha256()
		m.update(self.password.data.encode())
		passwd=m.hexdigest()
		return user if passwd== user.password else None
		
from flask.ext.login import login_user, current_user
from flask import request

@app.route("/login/",methods=("GET","POST",))
def login():
	f=LoginForm()
	fs = SearchForm()
	if not f.is_submitted():
		f.next.data=request.args.get("next")
	elif f.validate_on_submit():
		user=f.get_authenticated_user()
		if user:
			login_user(user)
			next=f.next.data or url_for("home")
			return render_template("login.html",form=f,fs=fs,testo=15)
			
		else:
			return render_template("login.html",form=f,fs=fs,testo=18)
		
	return render_template("login.html",form=f,fs=fs)
		


from flask.ext.login import logout_user		


@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for('home'))

@app.route("/ajout/<idvin>/<pagevin>/<namevin>/<imagevin>")
@app.route("/ajout/<name>/<idvin>/<pagevin>/<namevin>/<imagevin>")
def AjouterPannier(name,idvin,pagevin,namevin,imagevin):
	add_pannier(name=name,idvin=idvin,pagevin=pagevin,namevin=namevin,imagevin=imagevin)
	return redirect(url_for('home'))

@app.route("/supprimer/<id>/")
def SupprimerPannier(id):
	supprimer_pannier(id=id)
	return redirect(url_for('home'))

@app.route("/Reset/<name>")
def ResetPannier(name):
	reset_pannier(name)
	return redirect(url_for('home'))
	
from sqlalchemy import exc

@app.route("/inscription/",methods=("GET","POST",))
def newuser():
	f=LoginForm()
	fs = SearchForm()
	if f.validate_on_submit():
		from .models import User
		from hashlib import sha256
		m=sha256()
		m.update(f.password.data.encode())
		u=User(username=f.username.data,password=m.hexdigest(),adressemail=f.adressemail.data,grade="membre",pseudo=f.pseudo.data,prenom=f.prenom.data,nom=f.nom.data)
		try:
			db.session.add(u)
			db.session.commit()
		except exc.SQLAlchemyError:
			pass
			return render_template("inscription.html",form=f,fs=fs,testo=5)
		return render_template("inscription.html",form=f,fs=fs,testo=4)
	else:
		return render_template("inscription.html",form=f,fs=fs)
	
@app.route("/inscription")
def inscription():
	fs = SearchForm()
	return render_template("inscription.html", title="Inscription",fs=fs)
	#return render_template("inscription.html",form=f)

from .models import VIN

from werkzeug import secure_filename
from flask.ext.uploads import IMAGES
import os


class AjoutForm(Form):
	id=HiddenField('id')
	page=HiddenField('page')
	name=StringField('Nom',[Required()])
	country=SelectField(u'Country', choices=[('France', 'France'),('Espagne', 'Espagne'),('Italie', 'Italie'),('Royaume-uni', 'Royaume-uni')
	,('Canada', 'Canada'),('Etats-Unis', 'Etats-Unis'),('Chine', 'Chine'),('Japon', 'Japon')])
	image = FileField('Image')
	category=StringField('Category',[Required()])
	quantity=StringField('Quantity',[Required()])
	region=StringField('Region',[Required()])
	varietal=StringField('Varietal',[Required()])
	vintage=StringField('Vintage')
	
	def allowed_file(self, filename):
		return '.' in filename and filename.rsplit('.', 1)[1] in IMAGES
	
	def validate(self) :
		rv = Form.validate(self)
		if self.category.data=='' :
			self.category.errors.append("Veuillez choisir une catégorie.")
			return False
		file = request.files['image']
		if file.filename!="" :
			if not self.allowed_file(file.filename) :
				self.image.errors.append("Veuillez entrer une image valide.")
				return False
		if not rv:
			return False
		return True	

	def __init__(self, *args, **kwargs):	#reparaction du validate on submit
		kwargs['csrf_enabled'] = False
		super(AjoutForm, self).__init__(*args, **kwargs)
    
from werkzeug import secure_filename
	
@app.route('/save/newvin', methods=['GET','POST'])
@login_required
def save_newvin():
	form = AjoutForm(request.form)
	fs = SearchForm()
	id=get_last_vin()
	if form.validate_on_submit():
		id+=1
		file = request.files['image']
		if file.filename!="" :
			filename = secure_filename(file.filename)
			file_path = os.path.join(app.config['UPLOAD_IMG_VIN_FOLDER'], filename)
			file.save(file_path)
		else :
			filename = "default.jpg"
		
		if form.vintage.data=="" :
			annee=None
		else :
			annee=form.vintage.data
			new_vin = VIN(category=form.category.data,
					country=form.country.data,
					image=filename,
					name=form.name.data,
					quantity=form.quantity.data,
					region=form.region.data,
					varietal=form.varietal.data,
					vintage=form.vintage.data
					)
		flash("Vin ajouté","success")
		db.session.add(new_vin)
		db.session.commit()
		return redirect(url_for('home'))
	return render_template('gestionvin.html', form=form, fs=fs)


@app.route('/Supprimer/<id>/<num>')
@login_required
def suppr_vin(id,num):
	supprimerVin(id)
	a=get_nbpage()
	if a<int(num):
		return redirect(url_for('home', num_page=a))
	else:
		return redirect(url_for('home', num_page=int(num)))


class SearchForm(Form):
    names=StringField('name',[Required()])

@app.route('/search/vin',methods=['GET','POST'])
def search_vin():
	form=SearchForm(request.form)
	if form.validate_on_submit():
		return redirect(url_for('search_results', query = form.names.data))
		#~ return render_template('affichevin.html',id=VIN.id,page=get_nbpage,fs=form)
	return render_template('home.html',fs=form)
	
@app.route('/search/results/<query>')
def search_results(query):
	form=SearchForm()
	results = VIN.query.filter(VIN.name.startswith(query)).all()
	return render_template('home.html',fs=form,query = query,vins=results,num=1,nbpage=1)
	




#~ @app.route("/search/",methods=("GET","POST",))
#~ def search():
	#~ f=SearchForm(request.form)
	#~ if f.validate_on_submit():
		#~ search_str =str(f.names.data)
		#~ results = VIN.query.filter(VIN.name==search_str).all()
		#~ return render_template('search.html',form=f,test=4,query=f.names.data,search_str=search_str,results=results)
		#~ return redirect(url_for('search_results', query = form.names.data))
	#~ return render_template('search.html',form=f,test=5)
#~ 
	#~ 
@app.route("/profil")
@login_required
def profil():
	fs = SearchForm()
	return render_template("profil.html", title="profil",fs=fs)

@app.route("/editerprofil")
@login_required
def editprofil():
	a = current_user
	fs = SearchForm()
	f = LoginForm(username=a.username,adressemail=a.adressemail,pseudo=a.pseudo,prenom=a.prenom,nom=a.nom)
	return render_template("editerprofil.html",form=f,fs=fs)


class LoginFormdeux(Form):
	username=StringField('Username')
	password=PasswordField('Password')
	adressemail= StringField('adressemail')
	pseudo=StringField('pseudo')
	prenom=StringField('prenom')
	nom=StringField('nom')
	next= HiddenField()
	
	def get_authenticated_user(self):
		user= User.query.get(self.username.data)
		if user is None:
			return None
		m=sha256()
		m.update(self.password.data.encode())
		passwd=m.hexdigest()
		return user if passwd== user.password else None

@app.route("/save/profil/",methods=("POST",))
@login_required
def save_profil():
	a=None
	fs = SearchForm()
	f= LoginFormdeux()
	if f.validate_on_submit():
		print("slzl")
		a=current_user
		a.adressemail=f.adressemail.data
		a.pseudo=f.pseudo.data
		a.prenom=f.prenom.data
		a.nom=f.nom.data
		db.session.commit()
		#~ return redirect("http://127.0.0.1:5000/"+f.page.data)
		return redirect(url_for('profil'))
	return render_template("editerprofil.html",form=f,fs=fs)


